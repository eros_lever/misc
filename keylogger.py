#!/usr/bin/python

import os, sys, re, subprocess, shlex, pty, time, json

keycodes_file = os.path.join( os.path.dirname( __file__ ), "keycodes.json" )
if not os.path.exists(keycodes_file):
	f = open( "/usr/include/X11/keysymdef.h" )
	content = f.read()
	f.close()
	keycodes = re.findall( r'(?<=XK_)(\w+)\s+0x(?i)([\da-f]{4,})(?=\s)', content )
	keysym = {}
	for keyname, keycode in keycodes:
		if not keycode[:2] in ("ff","fe"):
			try:
				keysym[ keyname ] = unichr( int( keycode, 16 ) )
			except:
				#print "Unable to decode keysym: %s %s" % (keyname, keycode)
				pass
	f = open( keycodes_file, "w" )
	json.dump( keysym, f, indent=4 )
	f.close()
else:
	handle = open( keycodes_file )
	keysym = json.load( handle )
	handle.close()

stackable ={
	"Alt_L":"Alt",
	"Alt_R":"Alt",
	"Control_L":"Ctrl",
	"Control_R":"Ctrl",
	"Shift_L":"Shift",
	"Shift_R":"Shift",
	"ISO_Level3_Shift":"AltGr",
	"Super_L":"Win",
	"Super_R":"Win"
}


( output, _ ) = subprocess.Popen( shlex.split( "xinput list" ), stdout=subprocess.PIPE ).communicate()
dev_id = re.findall( r'AT .*id=(\d+)', output )

assert len(dev_id) == 1
dev_id = dev_id[ 0 ]

print "Using device ID: %s" % dev_id 

( output, _ ) = subprocess.Popen( shlex.split( "xmodmap -pke" ), stdout=subprocess.PIPE ).communicate()
key_map = {}
for line in output.splitlines():
	if not line.count("=") > 0:
		continue
	( keycode, value ) = line.split("=",1)
	( keycode, value ) = ( keycode.strip().split()[-1], value.strip().split() )
	if value:
		key_map[ keycode ] = value

def read( fd ):
	stdin = os.fdopen( fd )
	stdout = open( "/tmp/keylog", "w" )

	stack = []
	last = time.time()
	last_was_release = False
	while True:
		line = stdin.readline()
		if line == "":
			break
		line = line[:-1]
		if line:
			( _, event, key ) = line.split()
			if not key_map.has_key( key ):
				continue
			keycode = key
			key = key_map[ key ]
			stack_varies_key = len( key ) > 4
			stack_string = set( [ stackable[ k ] for k in stack ] )
			index = 0
			if stack_varies_key and not set( ["Alt","Ctrl"] ) & stack_string:
				if "Shift" in stack_string:
					index += 1
					stack_string.remove( "Shift" )
				if "AltGr" in stack_string:
					index += 4
					stack_string.remove( "AltGr" )
			key = key[ index ]
			if keysym.has_key( key ): # and not stackable.has_key( key ) and not len(key.encode("utf8")) > 2:
				key = keysym[ key ]
			to_log = False
			if event == "press":
				if stackable.has_key( key ):
					stack.append( key )
				else:
					to_log = True
			elif event == "release":
				if key in stack:
					stack.remove( key )
			if to_log:
				if time.time() - last > 1.5:
					stdout.write( "\n" )
				last = time.time()
				key = " + ".join( list( stack_string ) + [key] )
				if len( key ) > 1:
					key = " [%s] " % key
				stdout.write( key.encode( "utf-8" ) )
				stdout.flush()

pty.spawn( shlex.split( "xinput test %s" % dev_id ), read )
